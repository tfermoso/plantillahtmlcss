<?php 
function wd_load_script() {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', get_template_directory_uri().'/js/jquery-1.7.1.min.js');
    wp_register_script('tinycarousel', get_template_directory_uri() . '/js/jquery.tinycarousel.min.js', 'jquery');
    wp_register_script('custom', get_template_directory_uri() . '/js/custom.js', 'jquery');
 
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'tinycarousel' );
    wp_enqueue_script( 'custom' );
}    
 
add_action('wp_enqueue_scripts', 'wd_load_script');


/*------------------------------------------------------------*/
/*   Registrar Menus WP3.0+
/*------------------------------------------------------------*/
   if ( function_exists( 'register_nav_menus' ) )
   {
      register_nav_menus(
    	array(
        'main-nav'   => 'Main Navigation'
        
    )
);
   }

 ?>

