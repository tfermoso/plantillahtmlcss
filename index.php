<?php get_header() ?>



	<body class="">
	<div>
		
		<?php 
			echo get_template_directory_uri()."<br>";
			echo body_class();
		 ?>

	</div>
		
		<!-- WRAPPER -->
		<div id="wrapper" class="clearfix">
			
			<!-- NAV#TOP-MENU -->
			<nav id="top-menu" role="navigation" class="clearfix">
			
				<!-- ul Menú -->
				<?php wp_nav_menu(
					array(
			          'theme_location' => 'MenuPrincipal', 
			          'container' => 'false' 
          			));  ?>
				<!-- final ul Menú-->
			
				<!-- .Top-search -->
				<div class="top-search">
					<form  method="get" id="searchform" action="#">
						<input type="text" value="" name="s" id="s" placeholder="Buscar..." />
						<button id="searchsubmit">Go</button>
					</form>
				</div>
				<!-- Final .Top-search  -->
			
			</nav>
			<!-- FINAL NAV#TOP-MENU -->
			
			
			
			<!-- HEADER -->
			<header id="header" class="clearfix">
				
				<!-- #slider-wrapper -->
				<div id="slider-wrapper">
					
					<!-- .viewport -->
					<div class="viewport clearfix">
					
						<div>
							<ul class="overview">
								<li><a href="#" title=""><img src="images/tmp/img-slider-1.jpg" title="" alt=""></a></li>
								<li><a href="#" title=""><img src="images/tmp/img-slider-2.jpg" title="" alt=""></a></li>
								<li><a href="#" title=""><img src="images/tmp/img-slider-3.jpg" title="" alt=""></a></li>
								<li><a href="#" title=""><img src="images/tmp/img-slider-4.jpg" title="" alt=""></a></li>
								<li><a href="#" title=""><img src="images/tmp/img-slider-5.jpg" title="" alt=""></a></li>
							</ul>
						</div>
						
					</div>
					<!-- Final .viewport -->
					
					<!-- .slider-controls -->
					<div class="slider-controls">
						
						<!-- .pager -->
						<ul class="pager">
							<li><a rel="0" class="pagenum" href="#">1</a></li>
							<li><a rel="1" class="pagenum" href="#">2</a></li>
							<li><a rel="2" class="pagenum" href="#">3</a></li>
							<li><a rel="3" class="pagenum" href="#">4</a></li>
							<li><a rel="4" class="pagenum" href="#">5</a></li>
						</ul>
						<!-- Final .pager -->
						
						<!-- .button-left , .button-right -->
						<div class="button-left">
							<a class="buttons prev" href="#">left</a>
							<span></span>
						</div>
						
						<div class="button-right">
							<a href="#" class="buttons next">right</a>
							<span></span>
						</div>
						<!-- Final .button-left , .button-right -->
						
					</div>
					<!-- Final .slider-controls -->
					
				</div>					
				<!-- Final #slider-wrapper -->
				
				<!-- #logo -->
				<h1 id="logo">
					<a href="#" title="">Web Miusic | theme Wordpress</a>
				</h1>
				<!-- Final #logo -->
				
				<!-- .vermasVideos -->
				<div class="vermasVideos"><a href="#">Ver Más videos</a></div>
				<!-- Final .vermasVideos -->
				
			</header>
			<!-- FINAL HEADER -->
			
			
			
			<!-- #MAIN -->
			<div id="main" class="clearfix">
				
				<!-- .main-content -->
				<div class="main-content clearfix">
				
					<!-- #content -->
					<section id="content" role="main">
					
						<!-- .Posts -->
						<article class="posts">
							
							<!-- .entry-header -->
							<header class="entry-header">
							
								<div class="entry-titulo">
									<h2><a href="#">EMINEM ARRASA EN ESTADOS UNIDOS</a></h2>
								</div>
								
								<div class="MetaDatos">
									<span class="fecha">Jun 30th, 2010</span>
									<span>Categoría:<a href="#">Notícias Musciales</a></span>
									<span>Autor: <a href="#">JordanoPolanco</a></span>
								</div>
								
							</header>
							<!-- Final .entry-header -->
							
							<hr>
							
							<!-- Imagen del post -->
							<figure>
								<a href="#"><img src="images/tmp/post.jpg" title="" alt=""></a>
							</figure>
							<!-- Final Imagen del post -->
							
							<!-- .entry-content -->
							<div class="entry-content">
								<p>
									Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. 
									Separated they live in <a href="#">Bookmarksgrove right at the coast of the Semantics</a>, a large language ocean. 
									A small river named Duden flows by their place and supplies it with the necessary regelialia. 
									It is a paradisematic country, in which roasted parts of sentences fly into your mouth. 
								</p>
							</div>
							<!-- Final .entry-content -->
							
						</article>
						<!-- Final .posts -->
					
					</section>
					<!-- final #content -->
					
					
					<!-- #sidebar -->
					<aside id="sidebar" role="complementary">
						
						<!-- Widget Últimas Noticias -->
						<aside class="widget" id="ultimas-noticias">
							<h3>Últimas Noticias</h3>
							<ul>
								<li>
									<a href="#">Alejandro Sanz</a>
									<p>Miércoles - 8-09-2010</p>
									<p>Palacio de los Deportes Madrid</p>
								</li>
								
								<li>
									<a href="#">Alejandro Sanz</a>
									<p>Miércoles - 8-09-2010</p>
									<p>Palacio de los Deportes Madrid</p>
								</li>
								
								<li>
									<a href="#">Alejandro Sanz</a>
									<p>Miércoles - 8-09-2010</p>
									<p>Palacio de los Deportes Madrid</p>
								</li>
							</ul>
						</aside>
						<!-- Final Widget Últimas Noticias -->
						
						
						<!-- Widget Últimos Videos -->
						<aside class="widget" id="ultimos-videos">
							<h3>Últimas Noticias</h3>
							<ul>
								<li class="clearfix">
									<img src="images/tmp/list-video1.jpg" alt="">
									<p><a href="#">Mi princesa</a></p>
									<p>David Bisbal</p>
								</li>
								
								<li class="clearfix">
									<img src="images/tmp/list-video2.jpg" alt="">
									<p><a href="#">Desde Cuando</a></p>
									<p>Alejandro Sanz</p>
								</li>
								
								<li class="clearfix">
									<img src="images/tmp/list-video3.jpg" alt="">
									<p><a href="#">Mientes</a></p>
									<p>Camila</p>
								</li>

								<li class="clearfix">
									<img src="images/tmp/list-video4.jpg" alt="">
									<p><a href="#">Bad Romance</a></p>
									<p>Lady Gaga</p>
								</li>								
							</ul>
						</aside>
						<!-- Final Widget Últimos Videos -->
						
						
						<!-- Widget Contáctenos -->
						<aside class="widget">
							<h3>Contáctenos</h3>
							<form id="wiget-contact" action="#" method="post" name="Widget-Contact">
								<p><input id="form-nombre" type="text" placeholder="Nombre" name="Nombre"></p>
								<p><input id="form-email" type="email" placeholder="Email" name="Email"></p>
								<p><textarea id="form-mensage" cols="0" rows="8" placeholder="Su Mensaje..." name="Mensaje"></textarea></p>
								<p><button id="form-enviar">Enviar</button></p>
							</form>
						</aside>
						<!-- Final Widget Contáctenos -->
						
					</aside>
					<!-- Final #sidebar -->
					
				</div>
				<!-- final .main-content -->
				
				
			</div>
			<!-- FINAL #MAIN -->
			
		
			
			<!-- #FOOTER -->
			<footer id="footer" class="clearfix" role="contentinfo">
				
				<!-- .Copy -->
				<p class="copy">&copy;2010 Music.com. All rights reserved.</p>
				<!-- Final .Copy -->
				
				<!-- Menu Footer -->
				<ul>
					<li><a href="#">Inicio</a></li>
					<li><a href="#">Contácto</a></li>
					<li><a href="#">Descargas</a></li>
					<li><a href="#">Manuales</a></li>
				</ul>
				<!-- Final Menu Footer -->
				
				<!-- .GoTop -->
				<a href="#" class="goTop">Ir Arriba</a>
				<!-- Final .GoTop -->
				
			</footer>
			<!-- FINAL #FOOTER -->

		</div>
		<!-- FINAL WRAPPER -->
	
		
		<script>
		$(document).ready(function()
			{
			//Slider Show
				var pager = true;//ESTABLECE A FALSE PARA DESACTIVAR LA PAGINACIÓN.
			
				var oSlider = $('#slider-wrapper');
				if(oSlider.length > 0)
				{
					oSlider.tinycarousel(
					{ 	pager: true, //paginación
						interval: true, //Auto Play
						duration: 300 //retardo de pase de imagen
					});
				}
			});
		</script>
		<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->	
	</body>
</html>
